import NeoGrid from "./modules/grid";

const App = () => {
  return (
      <NeoGrid />
  );
}

export default App;
