import { IDateFilterParams, ValueFormatterParams } from "ag-grid-community";

export const dateFilterParams: Pick<IDateFilterParams, "comparator"> = {
  comparator: (filterLocalDateAtMidnight: Date, cellValue: string) => {
    const cellValueDate = new Date(cellValue);

    if (filterLocalDateAtMidnight.getTime() === cellValueDate.getTime()) {
      return 0;
    }
    if (cellValueDate < filterLocalDateAtMidnight) {
      return -1;
    }
    if (cellValueDate > filterLocalDateAtMidnight) {
      return 1;
    }

    return 0;
  },
};

/**
 * Accepts a value from the grid, and formats the date value into MMM DD, YYYY format
 * 
 * @param params 
 * @returns 
 */
export const dateFormatter = (params: ValueFormatterParams) => {
  const dateValue = new Date(params.value);
  return dateValue.toLocaleDateString("en-US", { month: "short", day: "numeric", year: "numeric" });
}

/**
 * Accepts a value from the grid, and formats the display value from it
 * n/a - returns empty
 * Y - returns Yes
 * N - returns No
 * 
 * @param params 
 * @returns 
 */
export const hazardValueDisplayFormatter = (params: ValueFormatterParams) => {
  if (params.value === "n/a") return "";

  return (params.value === "Y") ? "Yes" : "No";
}