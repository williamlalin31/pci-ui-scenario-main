import { AgGridReact } from "ag-grid-react";
import { ColDef } from "ag-grid-community";
import data from "../../near-earth-asteroids.json";
import "ag-grid-community/styles/ag-grid.css";
import "ag-grid-community/styles/ag-theme-alpine.css";
import { useRef } from "react";
import { dateFilterParams, dateFormatter, hazardValueDisplayFormatter } from "./helpers";

import "./index.css";

const columnDefs: ColDef[] = [
  { field: "designation", headerName: "Designation", sortable: true, filter: "agTextColumnFilter" },
  { field: "discovery_date", headerName: "Discovery Date", sortable: true, filter: "agDateColumnFilter", filterParams: dateFilterParams, valueFormatter: dateFormatter },
  { field: "h_mag", headerName: "H (mag)", sortable: true, filter: "agNumberColumnFilter" },
  { field: "moid_au", headerName: "MOID (au)", sortable: true, filter: "agNumberColumnFilter" },
  { field: "q_au_1", headerName: "q (au)", sortable: true, filter: "agNumberColumnFilter" },
  { field: "q_au_2", headerName: "Q (au)", sortable: true, filter: "agNumberColumnFilter" },
  { field: "period_yr", headerName: "Period (yr)", sortable: true, filter: "agNumberColumnFilter" },
  { field: "i_deg", headerName: "Inclination (deg)", sortable: true, filter: "agNumberColumnFilter" },
  { field: "pha", headerName: "Potentially Hazardous", sortable: true, filter: "agTextColumnFilter", valueFormatter: hazardValueDisplayFormatter },
  { field: "orbit_class", headerName: "Orbit Class", enableRowGroup: true, sortable: true, filter: "agTextColumnFilter" },
];

const NeoGrid = (): JSX.Element => {
  const gridRef = useRef<AgGridReact<any>>(null);

  const handleClearFiltersAndSorters = () => {
    gridRef.current?.api.setFilterModel(null);
    gridRef.current?.columnApi.applyColumnState({
      defaultState: { sort: null }
    })
  }

  return (
    <div id="grid-table" className="ag-theme-alpine">
      <div id="title-container">
        <h1 id="title-desc">Near-Earth Object Overview</h1>
        <button id="reset-all-bttn" onClick={handleClearFiltersAndSorters}>Clear Filters and Sorters</button>
      </div>
      <AgGridReact
        ref={gridRef}
        rowData={data}
        columnDefs={columnDefs}
        rowGroupPanelShow={'always'}
      />
    </div>
  );
};

export default NeoGrid;
